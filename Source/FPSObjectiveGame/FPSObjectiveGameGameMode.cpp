// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FPSObjectiveGameGameMode.h"
#include "FPSObjectiveGameHUD.h"
#include "FPSObjectiveGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AFPSObjectiveGameGameMode::AFPSObjectiveGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSObjectiveGameHUD::StaticClass();
}

void AFPSObjectiveGameGameMode::MissionComplete(APawn* InstigatorPawn, bool IsMissionComplete)
{
	if (InstigatorPawn)
	{
		InstigatorPawn->DisableInput(nullptr);
		AActor* NewViewTarget = nullptr;
		TArray<AActor*> ReturnedActors;
		UGameplayStatics::GetAllActorsOfClass(this, SpectatingCameraClass, ReturnedActors);
		if (ReturnedActors.Num() > 0)
		{
			NewViewTarget = ReturnedActors[0];
			APlayerController* PC = Cast<APlayerController>(InstigatorPawn->GetController());
			if (PC)
			{
				PC->SetViewTargetWithBlend(NewViewTarget, 2.0f, EViewTargetBlendFunction::VTBlend_Cubic);
			}
		
		}
	}

	OnMissionComplete(InstigatorPawn, IsMissionComplete);
}
