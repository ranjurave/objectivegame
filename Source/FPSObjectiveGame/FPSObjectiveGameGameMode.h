// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSObjectiveGameGameMode.generated.h"

UCLASS(minimalapi)
class AFPSObjectiveGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPSObjectiveGameGameMode();

	void MissionComplete(APawn* InstigatorPawn, bool IsMissionComplete);

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void OnMissionComplete(APawn* InstigatorPawn, bool IsMissionComplete);

	UPROPERTY(EditdefaultsOnly, Category = "Spectating")
	TSubclassOf<AActor> SpectatingCameraClass;
};



