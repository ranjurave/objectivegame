// Fill out your copyright notice in the Description page of Project Settings.


#include "ExtractionZone.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "FPSObjectiveGameCharacter.h"
#include "FPSObjectiveGameGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

// Sets default values
AExtractionZone::AExtractionZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetRelativeScale3D(FVector(1.0f, 1.0f, 0.2f));
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = MeshComp;
	BoxCollision->SetupAttachment(MeshComp);

	MeshComp->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);     // Setting no collision for the Sphere mesh
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);     // Setting collision response to only query no blocking
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);         // Setting collision response to ignore all
	BoxCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);  // Turn on collision response for pawn and generate overlap events

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AExtractionZone::HandleOverlap);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &AExtractionZone::HandleOverlapEnd);

	BoxCollision->SetHiddenInGame(false);
	
}

// Called when the game starts or when spawned
void AExtractionZone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExtractionZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFPSObjectiveGameCharacter* OverlapCharacter = Cast<AFPSObjectiveGameCharacter>(OtherActor);
	if (OverlapCharacter)
	{
		if (OverlapCharacter->bIsCarryingObjective)
		{
			AFPSObjectiveGameGameMode* GM = Cast<AFPSObjectiveGameGameMode>(GetWorld()->GetAuthGameMode());
			GM->MissionComplete(OverlapCharacter, true);

			//UE_LOG(LogTemp, Warning, TEXT("Extract the objective...."));
		}
		else
		{
			UGameplayStatics::PlaySound2D(this, ObjectiveMissingSound);
			//UE_LOG(LogTemp, Warning, TEXT("Pick up the objective...."));
		}
	}
}

void AExtractionZone::HandleOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//TODO stop audio when exit the extraction zone
}

