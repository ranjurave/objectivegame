// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupActor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FPSObjectiveGameCharacter.h"


// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetupAttachment(MeshComp);

	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);     // Setting no collision for the Sphere mesh
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);     // Setting collision response to only query no blocking
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);         // Setting collision response to ignore all
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);  // Turn on collision response for pawn and generate overlap events
}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupActor::PlayEffects()
{
	FVector ScaleSize = FVector(2.0f, 2.0f, 2.0f);
	FRotator FXRotator = FRotator(0.0f, 0.0f, 0.0f);
	UGameplayStatics::SpawnEmitterAtLocation(this, PicksupFX, GetActorLocation(),FXRotator,ScaleSize);
}

void APickupActor::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	PlayEffects();

	AFPSObjectiveGameCharacter* OverlapCharacter = Cast<AFPSObjectiveGameCharacter>(OtherActor);

	if (OverlapCharacter)
	{
		OverlapCharacter->bIsCarryingObjective = true;
		Destroy();
	}
}