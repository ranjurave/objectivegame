// Fill out your copyright notice in the Description page of Project Settings.


#include "GuardAI.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "FPSObjectiveGameGameMode.h"
#include "GameFramework/Actor.h"

// Sets default values
AGuardAI::AGuardAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	PawnSensingComp->OnSeePawn.AddDynamic(this, &AGuardAI::SeenPlayer);
	PawnSensingComp->OnHearNoise.AddDynamic(this, &AGuardAI::HearPlayer);

	GuardState = EAIState::Idle;
}

// Called when the game starts or when spawned
void AGuardAI::BeginPlay()
{
	Super::BeginPlay();

	OriginalRotation = GetActorRotation();
}

// Called every frame
void AGuardAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGuardAI::SeenPlayer(APawn* SeenPawn)
{
	if (SeenPawn == nullptr)
	{
		return;
	}
	
	AFPSObjectiveGameGameMode * GM = Cast<AFPSObjectiveGameGameMode>(GetWorld()->GetAuthGameMode());

	GM->MissionComplete(SeenPawn, false);

	if (GuardState == EAIState::Alert)
	{
		return;
	}
	SetGuardState(EAIState::Alert);
	//DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 30.0f, 12, FColor::Blue,false,10.0f);
}

void AGuardAI::HearPlayer(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();
	FRotator NewRotation = FRotationMatrix::MakeFromX(Direction).Rotator();

	NewRotation.Pitch = 0;
	NewRotation.Roll = 0;
	SetActorRotation(NewRotation);

	GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientation);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientation, this, &AGuardAI::ResetOrientation, 5.0f);

	SetGuardState(EAIState::Suspitious);
	//DrawDebugSphere(GetWorld(), Location, 30.0f, 12, FColor::Red, false, 10.0f);
}

void AGuardAI::ResetOrientation()
{
	SetActorRotation(OriginalRotation);
	SetGuardState(EAIState::Idle);
}

void AGuardAI::SetGuardState(EAIState NewState)
{
	if (GuardState == NewState)
	{
		return;
	}
	GuardState = NewState;
	OnStateChange(NewState);
}

